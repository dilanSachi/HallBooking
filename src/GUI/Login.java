/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Login {
    public void jLogin(){
        
        JFrame frame2=new JFrame();
        
        JLabel loginHead=new JLabel("Login");
        loginHead.setBounds(125,50,100,40);
        loginHead.setFont(new Font("Courier New",Font.ITALIC,18));
        
        JLabel userName=new JLabel("Username : ");
        userName.setBounds(50,100,100,40);
        
        JLabel password=new JLabel("Password : ");
        password.setBounds(50,150,100,40);
        
        JTextField userNameText=new JTextField();
        userNameText.setBounds(125,100,100,40);
        
        JPasswordField passwordText=new JPasswordField();
        passwordText.setBounds(125,150,100,40);
        
        JButton registerBtn=new JButton("Register");
        registerBtn.setBounds(100,400,100,40);
        
        JButton loginBtn=new JButton("Login");
        loginBtn.setBounds(250,400,100,40);
        
        
        frame2.add(loginHead);
        frame2.add(userName);
        frame2.add(password);
        frame2.add(userNameText);
        frame2.add(passwordText);
        frame2.add(loginBtn);
        frame2.add(registerBtn);
        
        frame2.setSize(400,500);
        frame2.setLayout(null);
        frame2.setVisible(true);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        registerBtn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                frame2.setVisible(false);
                Register registerInterface=new Register();
                registerInterface.jRegister();
            }
        });
        loginBtn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                
                String sql="select Password,isadmin from users where Username='"+userNameText.getText()+"';";
                
                char[] passwordChar=passwordText.getPassword();
                String enteredPassword="";
                
                for(int i=0;i<passwordChar.length;i++){
                    enteredPassword=enteredPassword+passwordChar[i];
                }
                
                Database database=new Database();
                ResultSet rs=database.getData(sql);
                try {
                    rs.next();//cursor to the next line
                    String password = rs.getString("Password");
                    boolean isadmin=rs.getBoolean("isadmin");
                    //System.out.println(password);
                    //System.out.println(enteredPassword);
                    if(password.equals(enteredPassword) && isadmin){
                        frame2.setVisible(false);
                        HallAdminInterface hall=new HallAdminInterface();
                        hall.jHallInterface();
                    }else if(password.equals(enteredPassword)){
                        frame2.setVisible(false);
                        HallInterface hall=new HallInterface();
                        hall.jHallInterface();
                    }else{
                        JOptionPane.showMessageDialog(null, "Wrong login detail");
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                    JOptionPane.showMessageDialog(null, "Wrong login detail");
                }
                //System.out.println(sql);
                //String databasePassword=
                
                //if(userNameText.getText()==username&&passwordText==)
            }
        });
    }
}
