
package GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class HallAdminInterface {
    public void jHallInterface(){
        
        JFrame frame3=new JFrame();
        
        JLabel hallHead=new JLabel("ABC Halls");
        hallHead.setBounds(125,50,200,40);
        hallHead.setFont(new Font("Courier New",Font.ITALIC,18));
        
        String sql="select * from halls;";
        Database database=new Database();
        ResultSet rs=database.getData(sql);
        
        try {
            //rs.next();
            
            int rowcount = 0;
            if (rs.last()) {
              rowcount = rs.getRow();
              rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
            }
            //blnna epa
            ArrayList<Hall> hallList=new ArrayList<Hall>();
            rs.next();
            for(int i=0;i<rowcount;i++){
                Hall hall=new Hall(rs.getInt("hall_no"));
                hallList.add(hall);
                rs.next();
            }
            
            rs.beforeFirst();
            //
            System.out.println(rowcount);
            int i=1;
            int bounds=0;
            ButtonGroup bg=new ButtonGroup();
            while(rs.next()){
                
                JRadioButton r1=new JRadioButton("Hall"+(i)); 
                JLabel l1=new JLabel();
                l1.setText(rs.getString("seats"));
                r1.setName(Integer.toString(i));
                r1.setBounds(50,140+bounds,100,30);
                l1.setBounds(150,140+bounds,100,30);
                bg.add(r1);
                frame3.add(r1);
                frame3.add(l1);
                
                bounds=bounds+30;
                i=i+1;
                
                r1.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        frame3.setVisible(false);
                        AdminHallSelect hall=new AdminHallSelect();
                        hall.jLogin(Integer.parseInt(r1.getName()));
                    }
                    
                });
            }
        }catch(Exception e){
            System.out.println(e);
        }
        
        JLabel addHall =new JLabel("Enter amount of seats : ");
        addHall.setBounds(50,100,185,30);
        
        JTextField seatAmount=new JTextField();
        seatAmount.setBounds(185,100,30,30);
        
        JButton addHallBtn=new JButton("Add New Hall");
        addHallBtn.setBounds(225,100,130,30);
        frame3.add(addHallBtn);
        frame3.add(seatAmount);
        frame3.add(addHall);
        
        addHallBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                Database database=new Database();
                String query = "SELECT * from halls";
                ResultSet rs=database.getData(query);
                
                int hallCount=0;
                try {
                    if (rs.last()) {
                        hallCount = rs.getRow();
                        rs.beforeFirst();
                    } 
                } catch (SQLException ex) {
                    Logger.getLogger(HallAdminInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
                String sql = "INSERT INTO halls (hall_no, seats) values ('"+(hallCount+1)+"','"+seatAmount.getText()+"')";
                
                
                database.setData(sql);
                
                frame3.setVisible(false);
                jHallInterface();
                
            }
        });
        
        frame3.add(hallHead);
        
        frame3.setSize(400,500);
        frame3.setLayout(null);
        frame3.setVisible(true);
        frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    
}
    //seats kiyala type wenna one
    //
}
