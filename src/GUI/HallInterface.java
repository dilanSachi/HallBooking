
package GUI;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class HallInterface {
    public void jHallInterface(){
        
         JFrame frame3=new JFrame();
        
        JLabel hallHead=new JLabel("ABC Halls");
        hallHead.setBounds(125,50,100,40);
        hallHead.setFont(new Font("Courier New",Font.ITALIC,18));
        
        JLabel instruction =new JLabel("Tick the hall you want to check in.");
        instruction.setBounds(50,100,100,40);
        
        frame3.add(hallHead);
        frame3.add(instruction);
        
        frame3.setSize(400,500);
        frame3.setLayout(null);
        frame3.setVisible(true);
    }
    
}
