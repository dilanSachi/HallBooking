
package GUI;

import java.sql.Date;
import java.util.ArrayList;

public class Hall {
    private int seats;
    final private int hall_id;
    
    public Hall(int id){
        hall_id=id;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
}
