/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Namunu PC
 */
public class Database {
    
    private Connection con;
    Statement stmt;
    
    public Database(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
            stmt= con.createStatement();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void setData(String sql){
        try{
            int rs=stmt.executeUpdate(sql);
            if(rs==1){
                System.out.println("Success");
            }else{
                System.out.println("Unsuccess");
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
    public ResultSet getData(String sql){
        ResultSet rs = null;
        try{
            rs=stmt.executeQuery(sql);
        }catch(Exception e){
            System.out.println(e);
        }
        return rs;
    }
    
}
